# README #

### Standard framework for testing and deployment of an angular site. ###

* Quick summary

* v1.0

### SET UP LOCAL VERSION ###

* Ensure that you have nodejs/npm, and bower installed globally

    -Windows Users-

        ~navigate to https://nodejs.org/download/
        ~run the download file to  put node and npm on your system
        ~enter command line and perform the following commands:

            npm install bower -g

    -Linux Users-

        ~install nodejs using your distro's package manager (e.g. apt-get)
        ~perform the following commands:

            npm install bower -g

* Clone the repository

            git clone https://bitbucket.org/jminyon/quoted.git

* Enter the repository directory (cd quoted) and run the following commands

            npm install
            bower install

### SET UP ENVIRONMENT ###

* Ensure the following are running in a command line.

    -GULP Tasks

            gulp default

* The window will open automatically at this point and show further instruction.

* You can alternatively run everything from inside of an atom environment

    -Install Git's Editor --ATOM

            https://atom.io

    -Install the plugin

            gulp-task-launcher written by booms8 -Conor Freeland(summanoid)

    -Run Tasks

            ctrl + alt + g (toggle service on/off)
