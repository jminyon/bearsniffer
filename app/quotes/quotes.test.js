describe('quotes', function() {

    describe('modules', function() {
        var module;

        beforeEach(function() {
            module = angular.module('app');
        });

        it('exists', function() {
            expect(module).not.toEqual(null);
        });
        it('also exists', function() {
            expect(module.quotes).not.toEqual(null);
        });
        it('also exists', function() {
            expect(module.users).not.toEqual(null);
        });
        it('also exists', function() {
            expect(module.sharedFctr).not.toEqual(null);
        });
    });

    /*describe('quotes module controllers', function() {
        var $controller;
        var $uibModal;
        var $uibModalInstance;

        beforeEach(angular.mock.module('quotes'));
        beforeEach(angular.mock.inject(function(_$controller_, _$httpBackend_) {
            $controller = _$controller_;
            $httpBackend = _$httpBackend_;
        }));

        it('quotesCtrl', function() {
            var $scope = {},
                q = $scope,
                controller = $controller('quotesCtrl', { $scope: $scope, $uibModal: $uibModal });

            q.sortDate('test');
            expect(q.sort).toEqual('test');

            q.refreshQuotes();
            expect(q.sort).toEqual('-id');
        });

        it('newQuoteCtrl', function() {
            var $scope = {},
                users,
                q = $scope,
                controller = $controller('newQuoteCtrl', { $scope: $scope, $uibModalInstance: $uibModalInstance, users: users });

            q.currentUser('test1');
            expect(q.userChoice).toEqual('test1');

            q.currentUser('test2');
            expect(q.userChoice).toEqual('test2');
        });

        it('editQuoteCtrl', function() {
            var $scope = {},
                q = $scope,
                users,
                quote = {
                'text': 'hello',
                'user': 'wolverine'
                },
                controller = $controller('editQuoteCtrl', { $scope: $scope, $uibModalInstance: $uibModalInstance, users: users, quote: quote });

            expect(q.editQuote.user).toEqual('wolverine');
            expect(q.editQuote.text).toEqual('hello');

            q.currentUser('test begin');
            expect(q.editQuote.user).toEqual('test begin');

            q.currentUser('test end');
            expect(q.editQuote.user).toEqual('test end');
        });

    });*/

});
