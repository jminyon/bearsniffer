(function() {

    'use strict';

    module.exports = function newQuoteCtrl($scope, $uibModalInstance, usersFctr) {
        var self = $scope;

        self.users = [];
        self.userChoice = {
            'name': 'WHO?',
            'id': 0
        };
        self.newQuote = {};

        self.getUsers = function() {
            usersFctr.users.query(
                users => {
                    self.users = users;
                }
            );
        };

        self.getUsers();

/*This changes the user that will be passed into the new quote.*/
        self.currentUser = function(user) {
            self.userChoice.name = user.name;
            self.userChoice.id = user.id;
        };

/*This closes the modal and passes nothing*/
        self.cancelQuote = function() {
            $uibModalInstance.dismiss();
        };

/*This closes the modal and passes the new quote to the controller.*/
        self.saveQuote = function() {
            self.newQuote.userId = self.userChoice.id;
            $uibModalInstance.close(self.newQuote);
        };

    };

})();
