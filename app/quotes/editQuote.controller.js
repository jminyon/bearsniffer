(function() {

    'use strict';

    module.exports = function editQuoteCtrl($scope, $uibModalInstance, usersFctr, current) {
        var self = $scope;
        
        self.users = [];
        self.userPass = {
            id: current.user.id,
            name: current.user.name
        };
        self.editQuote = {
            'id': current.quote.id,
            'text': current.quote.text,
            'userId': current.quote.userId,
            'date': current.quote.date
        };
        self.getUsers = function() {
            usersFctr.query(
                users => {
                    self.users = users;
                }
            );
        };

        self.getUsers();
/*This changes the user that will be passed into the new quote.*/
        self.currentUser = function(choice) {
            self.userPass.name = choice.name;
            self.editQuote.userId = choice.id;
        };

/*This closes the modal and passes nothing*/
        self.cancelQuote = function() {
            $uibModalInstance.dismiss();
        };

/*This closes the modal and passes the new quote to the controller.*/
        self.saveQuote = function() {
            $uibModalInstance.close(self.editQuote);
        };

    };
})();
