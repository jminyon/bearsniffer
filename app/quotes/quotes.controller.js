(function() {

    'use strict';

    module.exports = function quotesCtrl($scope, $uibModal, $http, $window, $cookies, usersFctr, quotesFctr) {
        const self = $scope;
        let quoteUser = {};
        let backEndDomain = 'http://localhost:8080';
        let frontEndDomain = 'http://localhost:3001';

        self.sort = '-id';
        self.quotes = [];
        self.userDict = [];
        self.users =[];
        self.location = $window.location.href.split('?');

/*LOGIN BUTTON*/
        self.login = function() {
            $http.post(backEndDomain + '/APBackend/api/login').then(function(response) {
                $window.location.href = response.data;
            });
        };

        self.verify = function(qParams) {
            $http.get(backEndDomain + "/APBackend/api/login/?" + qParams).then(function(response) {
                console.log('Verify Response', response);
                if(response.data) {
                    self.loggedIn = true;
                    let expire = new Date();
                    //expire = expire.setSeconds(expire.getSeconds() + 20);
                    let expiration = expire.setHours(expire.getSeconds() + 20);
                    console.log('Cookie Created', expire);
                    console.log('Cookie Expires', expiration);

                    $cookies.put("login", response.data, {
                        expires: expiration.toString()
                    });
                }
            });
        };
/*CHECK BUTTON*/
        self.check = function(id) {
           let cId = $cookies.get("login");
           if (cId) {
               console.log('Cookie Id Get:', cId);
               var escapedId = $cookies.get("login")
               .split('+')
               .join('%2B')
               .split('/')
               .join('%2F');
           }
           return $http.get(backEndDomain + "/APBackend/api/login/check?id=" + escapedId).then(function(response) {
               self.realId = response.data;
               console.log('Check Response:', response);
           });
       };
/*USERS AND QUOTES INITIALIZATION*/
        function getUsersFromQuotes(quotes) {
            let idString = '';
            let uniqueIds = _.uniqBy(quotes, quote => quote.userId)
                .map(quote => quote.userId);
            for(let id of uniqueIds) {
                idString += 'id='+id+'&';
            }
            const users = usersFctr.query({param: idString});
            return users;
        }

        const init = function() {
            console.log('Logged In:', self.loggedIn);
            console.log('Location After:', self.location);
            if (self.location[1] && !self.loggedIn) {
                self.verify(self.location[1]);
            }

            quotesFctr.query(
                quotes => {
                    self.quotes = quotes;
                    console.log('Quotes: ', quotes);
                    return getUsersFromQuotes(self.quotes).$promise.then(users => {
                        self.users = users;
                        for(let user of users) {
                            self.userDict[user.id] = user;
                        }
                    });
                },
                error => {
                    console.log('ERROR FROM QUOTE QUERY', error);
                }
            ).$promise.then(function() {
                console.log('Logged In:', self.loggedIn);
                console.log('Location After:', self.location);
                if (self.location[1] && !self.loggedIn) {
                    self.verify(self.location[1]);
                }
            });
        };

        self.sortDate = function(choice) {
            self.sort = choice;
        };

        self.sortUser = function(user) {
            self.user = user;
            self.userChoice = user;
        };

        self.refreshQuotes = function() {
            self.sort = '-id';
            self.user = self.users.name;
            init();
        };

        self.quoteSelector = function(choice) {
            self.quoteChoice = {
                quote: choice,
                user: self.userDict[choice.userId]
            };
        };

        self.deleteQuote = function(choice) {
            quotesFctr.delete({id: choice.quote.id}).$promise
            .then(response => {
                init();
            });
        };

        self.editQuote = function(choice) {
            return $uibModal.open({
                templateUrl: 'quotes/editQuote.html',
                controller: 'editQuoteCtrl',
                resolve: {
                    current: function() {
                        return choice;
                    }
                }
            }).result.then(function(updatedQuote) {
                quotesFctr.update({id: choice.quote.id}, updatedQuote).$promise
                .then(response => {
                    init();
                });
            }, function() {
                init();
            });
        };

        self.newQuote = function() {
            return $uibModal.open({
                templateUrl: 'quotes/newQuote.html',
                controller: 'newQuoteCtrl'
            }).result.then(function(newQuote) {
                quotesFctr.save(newQuote).$promise
                .then(response => {
                    init();
                });
            });
        };
        init();
    };

})();
