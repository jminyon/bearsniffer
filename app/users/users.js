(function() {

'use strict';

var app = angular.module('users', []);

app.controller('usersCtrl', require('./users.controller'));

})();
