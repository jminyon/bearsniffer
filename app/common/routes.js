(function() {

    'use strict';

    module.exports = function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider.state('app', {
            url: '/',
            views: {
                'navigation': {
                    templateUrl: 'common/navigation.html'
                },
                'header': {
                    templateUrl: 'common/header.html',
                },
                'content': {
                    templateUrl: 'common/content.html'
                },
                'footer': {
                    templateUrl: 'common/footer.html'
                }
            }
        })
        .state('app.about', {
            url: 'about',
            views: {
                'header@': {
                    templateUrl: 'about/header.html'
                },
                'content@': {
                    templateUrl:'about/about.html'
                }
            }
        })
        .state('app.users', {
            url: 'users',
            views: {
                'header@': {
                    templateUrl: 'users/header.html',
                },
                'content@': {
                    templateUrl: 'users/users.html',
                    controller: 'usersCtrl'
                }
            }
        })
        .state('app.quotes', {
            url: 'quotes',
            views: {
                'header@': {
                    templateUrl: 'quotes/header.html',
                },
                'content@': {
                    templateUrl: 'quotes/content.html',
                    controller: 'quotesCtrl'
                }
            }
        });
    };

})();
