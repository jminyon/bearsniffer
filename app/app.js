(function() {

'use strict';

var app = angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'users',
    'quotes',
    'shared'
]);

app.config(require('./common/routes'));

require('./users/users');
require('./quotes/quotes');
require('./common/shared');


})();
