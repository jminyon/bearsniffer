/******************************************************************************/
/*These are the variables required to build.***********************************/
/******************************************************************************/

var gulp = require('gulp'),
    connect = require('gulp-connect'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    eslint = require('eslint'),
    protractor = require('gulp-protractor').protractor,
    webdriver_update = require("gulp-protractor").webdriver_update,
    webdriver_standalone = require("gulp-protractor").webdriver_standalone,
    server = require('karma').Server,
    watchify = require('watchify'),
    browserify = require('browserify'),
    sync = require('browser-sync').create(),
    source = require('vinyl-source-stream'),
    path = require('path'),
    del = require('del'),
    _ = require('lodash'),
    globby = require('globby'),
    run = require('run-sequence');

/******************************************************************************/
/*These are the variables to decide what to bundle.****************************/
/******************************************************************************/

var userCode = ['app/**/*.js', '!app/bower_components/**'],
    sAssets = ['app/styles/*.scss'],
    uiAssets = ['app/**/*.html', 'app/styles/fonts/**', 'app/styles/images/**', '!app/**/README.md', '!app/bower_components'],
    bowerAssets = ['app/bower_components/**'],
    depBundle = ['dist/app/**', '!dist/app/bundle_*'];


/******************************************************************************/
/*This is the initial task to run a full server that watches updates.**********/
/******************************************************************************/

gulp.task('default', function() {
    run('clean-dist', 'webupdate', 'watch', 'browserSync');
});

gulp.task('deploy', function() {
    run('clean-dep','depBundle');
});

/******************************************************************************/
/*These Tasks all cascade from 'local' and start a testing server.*************/
/******************************************************************************/

gulp.task('local', function() {
    connect.server({
        root: 'dist/app/',
        port: 8888
    });
});

gulp.task('browserSync', ['local'], function() {
    sync.init({proxy:'localhost:8888'});
});

/******************************************************************************/
/*These Tasks all delete, clean, or recompile the folders and server.**********/
/******************************************************************************/

gulp.task('webupdate', webdriver_update);

gulp.task('clean-dist', function() {
    del('./dist/**').then;
});

gulp.task('clean-dep', function() {
    del('./dep/**').then;
});

/******************************************************************************/
/*These Tasks bundle together the dist and dep folders when run.***************/
/******************************************************************************/

gulp.task('depBundle', function() {
    gulp.src(depBundle, {base: './dist'}).pipe(gulp.dest('./dep'));
});

gulp.task('ui-assets', function() {
    return gulp.src(uiAssets, {base: '.'})
    .pipe(gulp.dest('dist'));
});

gulp.task('bower-assets', function() {
    return gulp.src(bowerAssets, {base: '.'})
    .pipe(gulp.dest('dist'));
});

gulp.task('s-assets', function() {
    return gulp.src('./app/styles/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/app/styles'));
});

gulp.task('lint', function() {
    return gulp.src(userCode, {base: '.'})
    .pipe(eslint.parser(babel-eslint));
});

gulp.task('browserify', function() {
    return browserify('./app/app.js', {debug: true})
    .transform('babelify', {presets: ['es2015']})
    .transform('browserify-ngannotate')
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./dist/app'));
});

/******************************************************************************/
/*These Tasks control the tests to run.****************************************/
/******************************************************************************/

gulp.task('karma', function(done) {
    new server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, function() {
        done();
    }).start();
});

gulp.task('e2e', function() {
    gulp.src(["dist/app/bundle_e2e.js"])
    .pipe(protractor({
        configFile: "conf.js",
        args: ['--baseUrl', 'http://127.0.0.1:8888']
    }))
    .on('error', function(e) { throw e; });
});

/******************************************************************************/
/*These Tasks cascade from 'watch' and set up the files to watch.**************/
/******************************************************************************/

gulp.task('watch', ['watchify-karma-tests', 'watchify-e2e-tests', 'watchify-site', 'ui-assets', 's-assets', 'bower-assets'], function() {
    gulp.watch(uiAssets, ['ui-assets']).on('change', sync.reload);
    gulp.watch(sAssets, ['s-assets']).on('change', sync.reload);
});

gulp.task('watchify-site', function() {
    return watchifyCreator('./app/app.js', 'bundle.js', ['karma']);
});

gulp.task('watchify-karma-tests', function() {
    var bundleName = 'bundle_karma.js';
    var testFiles = globby.sync(['./app/**/*.test.js', '!./dist/app/' + bundleName]);

    return watchifyCreator(testFiles, bundleName, ['karma']);
});

gulp.task('watchify-e2e-tests', function() {
    var bundleName = 'bundle_e2e.js';
    var testFiles = globby.sync(['./app/tests/*.js', '!./dist/app/' + bundleName]);

    return watchifyCreator(testFiles, bundleName, ['e2e']);
});

function watchifyCreator(files, bundleName, tests) {
    var watchified = watchify(browserify(files, _.extend({debug: true}, watchify.args)));

    function update() {
        return watchified.bundle()
        .on('error', gutil.log.bind(gutil, 'Browserify error'))
        .pipe(source(bundleName))
        .pipe(gulp.dest('./dist/app'));
    }

    watchified.transform('babelify', {presets: ['es2015']})
    .transform('browserify-ngannotate')
    .transform('require-globify');

    watchified.on('update', function() {
        update();
        run(tests);
    });

    watchified.on('log', gutil.log);

    return update();
}
